<div align="center">
<img src="android-chrome-256x256.png" alt="blue pen">
<h1>Upload Sign and Save</h1>
<a href="LISCENSE">MIT</a>
</div>
<hr>

<div align="center">
<h4>You can try a live demo <a href="https://9yxzbn.csb.app/">here</a>.</h4>
</div>
<br>

## Description
I made this project for a homeschooled child whose printer was out of ink. It allows you to upload a document, sign it, then save it. There are no dependencies or frameworks. It's just HTML, CSS and Javascript. Maybe someone else can find it useful.

## Installation/Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/{YOUR REPO}
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
<a href="https://lesley-nicole.github.com"><img src="name-remix.svg" width="25%" alt="Lesley-Nicole"></a>

## License
MIT
